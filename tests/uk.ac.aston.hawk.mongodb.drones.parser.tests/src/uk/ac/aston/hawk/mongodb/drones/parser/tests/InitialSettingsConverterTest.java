package uk.ac.aston.hawk.mongodb.drones.parser.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import uk.ac.aston.hawk.mongodb.drones.parser.InitialSettingsConverter;
import uk.ac.aston.stormlog.DoubleListMeasurement;
import uk.ac.aston.stormlog.Log;
import uk.ac.aston.stormlog.Observation;

public class InitialSettingsConverterTest {

	@Test
	public void parseQLearning() throws IOException {
		File fJSON = new File("resources/initial.json");
		InitialSettingsConverter converter = new InitialSettingsConverter(fJSON);

		// Check the actions
		Log log = converter.getLog();
		assertEquals(5, log.getActions().size());

		// Check the measures
		assertFalse("There should be measures defined", log.getMeasures().isEmpty());

		// Check the measurements, as well as the parsing of the tuple lists
		Observation obs = log.getObservations().get(0);
		DoubleListMeasurement dronePos = (DoubleListMeasurement)
			obs.getFirstMeasurement(InitialSettingsConverter.MEASURE_INITIAL_DRONEPOS);
		assertNotNull(dronePos);
		assertEquals(6, dronePos.getValue().size());
		assertEquals(55.0d, dronePos.getValue().get(0), 1e-4);
		assertEquals(30.0d, dronePos.getValue().get(5), 1e-4);
	}

	@Test
	public void parseDQN() throws IOException {
		// TODO
	}
}
