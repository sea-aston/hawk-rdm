package uk.ac.aston.hawk.mongodb.drones.parser.tests;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

import uk.ac.aston.hawk.mongodb.drones.parser.InitialSettingsConverter;
import uk.ac.aston.hawk.mongodb.drones.parser.QTableConverter;
import uk.ac.aston.stormlog.Decision;
import uk.ac.aston.stormlog.IntegerMeasurement;
import uk.ac.aston.stormlog.Log;
import uk.ac.aston.stormlog.Observation;

public class QTableConverterTest {

	@Test
	public void parseQLearning() throws IOException {
		final File fJSON = new File("resources/initial.json");
		final InitialSettingsConverter converter = new InitialSettingsConverter(fJSON);
		final Log baseLog = converter.getLog();

		final File fQTableJSON = new File("resources/sarsa-qtablev2.json");
		final QTableConverter qTableConverter = new QTableConverter(baseLog, fQTableJSON);
		final Log log = qTableConverter.getLog();

		final Decision decision = log.getDecisions().get(0);
		assertEquals(InitialSettingsConverter.ACTION_LEFT, decision.getActionTaken().getName());
		assertEquals(0, decision.getActionBeliefs().get(1).getEstimatedValue(), 1e-4);
		final Observation obs = log.getObservations().get(0);
		assertEquals(0, ((IntegerMeasurement) obs.getFirstMeasurement(InitialSettingsConverter.MEASURE_EPISODE)).getValue());
		assertEquals(0, ((IntegerMeasurement) obs.getFirstMeasurement(InitialSettingsConverter.MEASURE_STEP)).getValue());
		assertEquals(0, ((IntegerMeasurement) obs.getFirstMeasurement(InitialSettingsConverter.MEASURE_DRONE)).getValue());
		assertEquals(10, ((IntegerMeasurement) obs.getFirstMeasurement(InitialSettingsConverter.MEASURE_REWARD_TOTAL)).getValue());
		assertEquals(5, ((IntegerMeasurement) obs.getFirstMeasurement(InitialSettingsConverter.MEASURE_REWARD_DRONE)).getValue());
	}
	
	@Test
	public void parseDQN() throws IOException {
		final File fJSON = new File("resources/initial-dqn.json");
		final InitialSettingsConverter converter = new InitialSettingsConverter(fJSON);
		final Log baseLog = converter.getLog();

		final File fQTableJSON = new File("resources/dqn-qtableV2.json");
		final QTableConverter qTableConverter = new QTableConverter(baseLog, fQTableJSON);
		if (qTableConverter.getLog() != null) {
			final Log log = qTableConverter.getLog();
		
			final Decision decision = log.getDecisions().get(0);
			assertEquals(InitialSettingsConverter.ACTION_DOWN, decision.getActionTaken().getName());
			assertEquals(0.24528709, decision.getActionBeliefs().get(2).getEstimatedValue(), 1e-4);

			final Observation obs = log.getObservations().get(0);
			assertEquals(0, ((IntegerMeasurement) obs.getFirstMeasurement(InitialSettingsConverter.MEASURE_EPISODE)).getValue());
			assertEquals(0, ((IntegerMeasurement) obs.getFirstMeasurement(InitialSettingsConverter.MEASURE_STEP)).getValue());
			assertEquals(0, ((IntegerMeasurement) obs.getFirstMeasurement(InitialSettingsConverter.MEASURE_DRONE)).getValue());
			assertEquals(10, ((IntegerMeasurement) obs.getFirstMeasurement(InitialSettingsConverter.MEASURE_REWARD_TOTAL)).getValue());
			assertEquals(5, ((IntegerMeasurement) obs.getFirstMeasurement(InitialSettingsConverter.MEASURE_REWARD_DRONE)).getValue());
			}
	}
}
