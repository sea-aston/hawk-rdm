package org.hawk.mongodb.drones;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.Socket;
import java.util.Collection;
import java.util.Iterator;

import org.bson.Document;
import org.eclipse.hawk.core.ICredentialsStore;
import org.eclipse.hawk.core.IModelIndexer;
import org.eclipse.hawk.core.VcsChangeType;
import org.eclipse.hawk.core.VcsCommit;
import org.eclipse.hawk.core.VcsCommitItem;
import org.eclipse.hawk.core.VcsRepositoryDelta;
import org.eclipse.hawk.core.ICredentialsStore.Credentials;
import org.eclipse.hawk.core.util.DefaultConsole;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class DroneMongoDatabaseTest {

	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();
	private static final String DB_NAME = "DQN_Data_Base";
	private static final String MONGO_URL = String.format(
		"mongo://localhost:27017/%s", DB_NAME
	);

	private ICredentialsStore credStore;
	private DroneMongoDatabase vcs = new DroneMongoDatabase();
	private MongoDatabase mongoDB;
	
	@Before
	public void setUp() throws Exception {
		// Keeps Tycho happy in GitLab
		assumeTrue(isPortInUse("localhost", 27017));

		// Clear collection between tests
		mongoDB = MongoClients.create().getDatabase(DB_NAME);
		getInitialSettingsCollection().deleteMany(new Document());
		getQTableCollection().deleteMany(new Document());

		credStore = mock(ICredentialsStore.class);
		when(credStore.get(anyString())).thenReturn(new Credentials("dummy", "dummy"));

		IModelIndexer indexer = mock(IModelIndexer.class);
		when(indexer.getConsole()).thenReturn(new DefaultConsole());
		when(indexer.getCredentialsStore()).thenReturn(credStore);

		vcs = new DroneMongoDatabase();
		vcs.init(MONGO_URL, indexer);
		vcs.run();
		assertTrue(vcs.isActive());
		assertFalse(vcs.isFrozen());
	}

	private MongoCollection<Document> getQTableCollection() {
		return mongoDB.getCollection(DroneMongoDatabase.Q_TABLE_COLLECTION);
	}

	private MongoCollection<Document> getInitialSettingsCollection() {
		return mongoDB.getCollection(DroneMongoDatabase.INITIAL_SETTINGS_COLLECTION);
	}

	@After
	public void tearDown() {
		if (vcs != null) {
			vcs.shutdown();
		}
	}

	@Test
	public void emptyDatabase() throws Exception {
		assertEquals("0", vcs.getCurrentRevision());
		assertTrue(vcs.getHumanReadableName().contains("Mongo"));
	}

	@Test
	public void oneStepDeltas() throws Exception {
		// Add just enough documents to emulate the part of the DB structure we will need
		insertInitialSettings();
		insertQTableDocs(0, 0);

		final String vcsCurrent = vcs.getCurrentRevision();
		assertTrue(vcsCurrent.startsWith("0_0_"));

		// Try to fetch the history from the "virtual" empty first revision. 
		assertDeltaWithInitialSettingsAdded(vcs.getDelta(DroneMongoDatabase.FIRST_REVISION));

		//The Hawk updater uses "negative" revision values to represent the fact that
		//it has never checked this repository before.
		 
		assertDeltaWithInitialSettingsAdded(vcs.getDelta("-3"));
		
		//This should only give the changed Q-table file
		assertEquals(1, vcs.getDelta(vcsCurrent).size());

		// Do similar checks, but with ranges
		VcsRepositoryDelta delta = vcs.getDelta(DroneMongoDatabase.FIRST_REVISION, DroneMongoDatabase.FIRST_REVISION);
		assertEquals(1, delta.size());
		assertEquals(1, delta.getCompactedCommitItems().size());
		
		delta = vcs.getDelta(DroneMongoDatabase.FIRST_REVISION, vcsCurrent);
		assertEquals(3, delta.size());
		assertDeltaWithInitialSettingsAdded(delta.getCompactedCommitItems());		

		delta = vcs.getDelta(vcsCurrent, vcsCurrent);
		assertEquals(1, delta.size());
		assertEquals(1, delta.getCompactedCommitItems().size());
	}
	
	@Test
	public void oneStepImport() throws Exception {
		// Add just enough documents to emulate the part of the DB structure we will need
		insertInitialSettings();
		insertQTableDocs(0, 0);
		final File tmpImport = tempFolder.newFile();

		File f = vcs.importFile(
			DroneMongoDatabase.FIRST_REVISION,
			"/" + DroneMongoDatabase.INITIAL_SETTINGS_DOCUMENT,
			tmpImport);
		Document d = parseFile(f);
		assertTrue(d.containsKey("example_key"));
		
		assertNull(vcs.importFile(DroneMongoDatabase.FIRST_REVISION,
				DroneMongoDatabase.stepFilePath(0, 0),
				tmpImport));
		assertNull(vcs.importFile(DroneMongoDatabase.FIRST_REVISION,
				DroneMongoDatabase.stepFilePath(0, 1),
				tmpImport));

		final String currentRevision = vcs.getCurrentRevision();
		final VcsRepositoryDelta fullHistory = vcs.getDelta(DroneMongoDatabase.FIRST_REVISION, currentRevision);

		Iterator<VcsCommit> itCommits = fullHistory.getCommits().iterator();
		itCommits.next();
		final String prevRevision = itCommits.next().getRevision();

		f = vcs.importFile(prevRevision, DroneMongoDatabase.stepFilePath(0, 0), tmpImport);
		d = parseFile(f);
		assertNotNull(d.get("episode"));
		assertNotNull(d.get("step"));
		assertNotNull(d.get("drone number"));

		f = vcs.importFile(currentRevision, DroneMongoDatabase.stepFilePath(0, 1), tmpImport);
		d = parseFile(f);
		assertNotNull(d.get("episode"));
		assertNotNull(d.get("step"));
		assertNotNull(d.get("drone number"));	
		}
	
	@Test
	public void twoStep() throws Exception {
		insertInitialSettings();
		insertQTableDocs(0, 0);
		insertQTableDocs(0, 1);
		insertQTableDocs(1, 0);

		VcsRepositoryDelta delta = vcs.getDelta(vcs.getFirstRevision(), vcs.getCurrentRevision());
		assertEquals(7, delta.size());

		Iterator<VcsCommit> itCommits = delta.getCommits().iterator();
		skip(itCommits, 3);
		final VcsCommitItem ep0step1 = itCommits.next().getItems().get(0);
		assertEquals(VcsChangeType.UPDATED, ep0step1.getChangeType());

		skip(itCommits, 1);
		final VcsCommit lastCommit = itCommits.next();
		final File tmpImport = tempFolder.newFile();
		final VcsCommitItem ep1step0 = lastCommit.getItems().get(0);
		assertEquals(VcsChangeType.ADDED, ep1step0.getChangeType());

		final File f = vcs.importFile(lastCommit.getRevision(), ep1step0.getPath(), tmpImport);
		final Document d = parseFile(f);
		assertNotNull(d.get("episode"));
		assertNotNull(d.get("step"));
		assertNotNull(d.get("drone number"));	
	}

	private static void skip(Iterator<?> itCommits, final int n) {
		for (int i = 0; i < n; i++) itCommits.next();
	}
	
	private Document parseFile(File f) throws IOException {
		try (FileReader fR = new FileReader(f); BufferedReader bR = new BufferedReader(fR)) {
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = bR.readLine()) != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
			}
			return Document.parse(sb.toString());
		}
	}

	private void insertInitialSettings() {
		getInitialSettingsCollection().insertOne(new Document()
			.append("example_key", "example_value")
		);
	}

	private void insertQTableDocs(int episode, int step) {	
		getQTableCollection().insertOne(new Document()
				.append("episode", episode)
				.append("step", step)
				.append("drone number", 0));
		getQTableCollection().insertOne(new Document()
				.append("episode", episode)
				.append("step", step)
				.append("drone number", 1));
		
		
	}

	private void assertDeltaWithInitialSettingsAdded(Collection<VcsCommitItem> delta) {
		Iterator<VcsCommitItem> itDelta = delta.iterator();
		assertTrue(itDelta.hasNext());

		boolean hasInitial = false;
		boolean hasTableD0 = false;
		boolean hasTableD1 = false;

		// There should be exactly two changes: both files should have been ADDED
		VcsCommitItem item = itDelta.next();
		assertEquals(VcsChangeType.ADDED, item.getChangeType());
		hasInitial = hasInitial || item.getPath().equals("/" + DroneMongoDatabase.INITIAL_SETTINGS_DOCUMENT);
		hasTableD0 = hasTableD0 || item.getPath().equals("/" + DroneMongoDatabase.Q_TABLE_COLLECTION + "_0_0.json");
		hasTableD1 = hasTableD1 || item.getPath().equals("/" + DroneMongoDatabase.Q_TABLE_COLLECTION + "_0_1.json");
		assertTrue(itDelta.hasNext());

		item = itDelta.next();
		assertEquals(VcsChangeType.ADDED, item.getChangeType());
		hasInitial = hasInitial || item.getPath().equals("/" + DroneMongoDatabase.INITIAL_SETTINGS_DOCUMENT);
		hasTableD0 = hasTableD0 || item.getPath().equals("/" + DroneMongoDatabase.Q_TABLE_COLLECTION + "_0_0.json");
		hasTableD1 = hasTableD1 || item.getPath().equals("/" + DroneMongoDatabase.Q_TABLE_COLLECTION + "_0_1.json");
		assertTrue(itDelta.hasNext());
		 
		item = itDelta.next();
		assertEquals(VcsChangeType.ADDED, item.getChangeType());
		hasInitial = hasInitial || item.getPath().equals("/" + DroneMongoDatabase.INITIAL_SETTINGS_DOCUMENT);
		hasTableD0 = hasTableD0 || item.getPath().equals("/" + DroneMongoDatabase.Q_TABLE_COLLECTION + "_0_0.json");
		hasTableD1 = hasTableD1 || item.getPath().equals("/" + DroneMongoDatabase.Q_TABLE_COLLECTION + "_0_1.json");
		assertFalse(itDelta.hasNext());

		assertTrue(hasInitial);
		assertTrue(hasTableD0);
		assertTrue(hasTableD1);
	}

	private boolean isPortInUse(String hostName, int portNumber) {
        try (Socket s = new Socket(hostName, portNumber)) {
            return true;
        }
        catch (Exception e) {
            return false;
        }
	}

}
