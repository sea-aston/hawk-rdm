# hawk-rdm

Components to extend RDM and other systems with historic explanation capabilities using [Hawk](https://www.eclipse.org/hawk/).

GitLab automatically builds new update sites from `master`.
To install the latest version, use this update site URL:

    https://dl.bintray.com/sea-group/hawk-rdm/1.2.0/

