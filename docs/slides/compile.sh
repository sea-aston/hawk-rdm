#!/bin/bash

JOB=hawk-mrt2018-slides

set -x

latexmk -pdf "$JOB"
cp "$JOB"{.pdf,-copy.pdf}
latexmk -C "$JOB"
latexmk -pdf "$JOB"
