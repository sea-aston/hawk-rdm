package uk.ac.aston.hawk.mongodb.drones.integrationtest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assume.assumeTrue;

import java.io.File;
import java.net.Socket;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.hawk.backend.tests.factories.IGraphDatabaseFactory;
import org.eclipse.hawk.core.IMetaModelResourceFactory;
import org.eclipse.hawk.core.IModelIndexer;
import org.eclipse.hawk.core.IModelResourceFactory;
import org.eclipse.hawk.core.model.IHawkMetaModelResource;
import org.eclipse.hawk.core.query.InvalidQueryException;
import org.eclipse.hawk.core.query.QueryExecutionException;
import org.eclipse.hawk.core.security.FileBasedCredentialsStore;
import org.eclipse.hawk.emf.EMFWrapperFactory;
import org.eclipse.hawk.emf.metamodel.EMFMetaModelResource;
import org.eclipse.hawk.emf.metamodel.EMFMetaModelResourceFactory;
import org.eclipse.hawk.graph.updater.GraphModelUpdater;
import org.eclipse.hawk.greycat.tests.LevelDBGreycatDatabaseFactory;
import org.eclipse.hawk.integration.tests.ModelIndexingTest;
import org.eclipse.hawk.timeaware.graph.TimeAwareIndexer;
import org.eclipse.hawk.timeaware.graph.TimeAwareModelUpdater;
import org.eclipse.hawk.timeaware.queries.TimeAwareEOLQueryEngine;
import org.hawk.mongodb.drones.DroneMongoDatabase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runners.Parameterized.Parameters;

import com.mongodb.client.model.Filters;

import uk.ac.aston.hawk.mongodb.drones.parser.MongoDroneResourceFactory;
import uk.ac.aston.stormlog.StormlogPackage;

/**
 * Integration test for the Mongo Drones support in Hawk. Requires having a
 * running Mongo server with the drones database.
 */
public class IntegrationTest extends ModelIndexingTest {

	private static final String DB_NAME = "DQN_Data_Base";

	private static final class MongoDronesModelSupportFactory implements IModelSupportFactory {
		@Override
		public IModelResourceFactory createModelResourceFactory() {
			return new MongoDroneResourceFactory();
		}

		@Override
		public IMetaModelResourceFactory createMetaModelResourceFactory() {
			return new EMFMetaModelResourceFactory();
		}
	}

	@Parameters(name = "{0}")
    public static Iterable<Object[]> params() {
    	return Arrays.asList(new Object[][] {
    		{ new LevelDBGreycatDatabaseFactory(), new MongoDronesModelSupportFactory() },
    	});
    }

	private TimeAwareEOLQueryEngine timeAwareQueryEngine;

	public IntegrationTest(IGraphDatabaseFactory dbFactory, IModelSupportFactory msFactory) {
		super(new File("."), dbFactory, msFactory);
	}

	@Before
	public void setUp() throws Exception {
		final EMFWrapperFactory wf = new EMFWrapperFactory();
		final EMFMetaModelResourceFactory rf = new EMFMetaModelResourceFactory();
		final Set<IHawkMetaModelResource> resources = new HashSet<>();
		resources.add(new EMFMetaModelResource(EcorePackage.eINSTANCE.eResource(), wf, rf));
		resources.add(new EMFMetaModelResource(StormlogPackage.eINSTANCE.eResource(), wf, rf));
		indexer.getMetaModelUpdater().insertMetamodels(resources, indexer);

		timeAwareQueryEngine = new TimeAwareEOLQueryEngine();
		indexer.addQueryEngine(timeAwareQueryEngine);
	}

	@Override
	protected GraphModelUpdater createModelUpdater() {
		return new TimeAwareModelUpdater();
	}

	@Override
	protected IModelIndexer createIndexer(File indexerFolder, FileBasedCredentialsStore credStore) {
		return new TimeAwareIndexer("test", indexerFolder, credStore, console);
	}

	@Test
	public void tryIndexing() throws Throwable {
		// Keeps Tycho happy in GitLab
		assumeTrue(isPortInUse("localhost", 27017));

		final DroneMongoDatabase vcs = new DroneMongoDatabase();
		vcs.init("mongo://localhost:27017/" + DB_NAME, indexer);
		vcs.setCurrentVersionFilter(Filters.and(
			Filters.lte("episode", 0),
			Filters.lte("step", 10)
		));
		vcs.run();
		indexer.addVCSManager(vcs, true);

		scheduleAndWait((Callable<?>) () -> {
			assertEquals("Three logs should exist: initial, drone 0, drone 1",
				3, timeAwareEOL("return Log.latest.all.size;"));
			assertEquals("There should be no proxy references",
				Collections.emptyList(), timeAwareEOL("return Model.proxiesNow;"));

			return null;
		});
	}

	protected Object timeAwareEOL(final String eolQuery) throws InvalidQueryException, QueryExecutionException {
		return timeAwareEOL(eolQuery, null);
	}

	protected Object timeAwareEOL(final String eolQuery, Map<String, Object> context) throws InvalidQueryException, QueryExecutionException {
		return timeAwareQueryEngine.query(indexer, eolQuery, context);
	}

	private boolean isPortInUse(String hostName, int portNumber) {
        try (Socket s = new Socket(hostName, portNumber)) {
            return true;
        }
        catch(Exception e) {
            return false;
        }
	}
}