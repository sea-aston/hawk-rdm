package org.hawk.mqtt;

import java.io.IOException;
import java.util.NavigableMap;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class MqttSubscriber {

	private final String topic;
	private final String broker;
	private final Runnable messageCallback;
	private int counterID =0;
	private MemoryPersistence persistence;
	private MqttClient mqttClient;
	private ConcurrentNavigableMap<Long, byte[]> messageLog;

	public MqttSubscriber(String broker, String port, String topic, Runnable messageCallback) {
		this.broker = "tcp://"+broker + ":" + port; 
		this.topic = topic;
		this.messageCallback = messageCallback;
	}

	public void run() throws IOException, InterruptedException, MqttException {
		persistence = new MemoryPersistence();
		messageLog = new ConcurrentSkipListMap<>();

		// Connect to the broker
		mqttClient = new MqttClient(broker, MqttClient.generateClientId(), persistence);
		MqttConnectOptions connOpts = new MqttConnectOptions();
		connOpts.setCleanSession(false);

		// Set Callbacks
		mqttClient.setCallback(new MqttCallback() {

			@Override
			public void connectionLost(Throwable cause) {
				// Called when the client lost the connection to the broker
				System.out.println("Connection to localhost broker lost! " + cause.getMessage());
			}

			@Override
			public void messageArrived(String topic1, MqttMessage message1) throws Exception {
				if (topic1.equals(topic)) {
					counterID ++; 
					System.out.println("CounterID: " + counterID);
					final long nanoTime = System.nanoTime();
					System.out.println("Received message at: " + nanoTime);
					messageLog.put(nanoTime, message1.getPayload());
					messageCallback.run();
					}
			}

			@Override
			public void deliveryComplete(IMqttDeliveryToken token) {
				// Called when a outgoing publish is complete
			}
		});

		// Conecting and subscribing to MQTT
		System.out.println("Connecting to broker: " + broker);
		mqttClient.connect(connOpts);
		System.out.println("Connected");
		System.out.println("Subscribing client to topic: " + topic);
		mqttClient.subscribe(topic, 0);
		System.out.println("Subscribed");
	}

	public NavigableMap<Long, byte[]> getLog() {
		return messageLog;
	}

	public String getTopic() {
		return topic;
	}

	public void shutdown() throws MqttException {
		mqttClient.unsubscribe(topic);
		mqttClient.close();
		persistence.close();
		messageLog.clear();

		mqttClient = null;
		persistence = null;
		messageLog = null;
	}

	public long getCurrentRevision() {
		if (messageLog.isEmpty()) {
			return 0;
		} else {
			return messageLog.lastKey();
		}
	}

}
