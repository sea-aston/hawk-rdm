package uk.ac.aston.log2repo.hawk;

import java.io.File;
import java.util.Collection;
import java.util.Collections;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.hawk.core.IFileImporter;
import org.eclipse.hawk.core.IModelResourceFactory;
import org.eclipse.hawk.core.model.IHawkModelResource;
import org.eclipse.hawk.emf.EMFWrapperFactory;
import org.eclipse.hawk.emf.model.EMFModelResource;

import uk.ac.aston.log2repo.SingleSliceLogConverter;
import uk.ac.aston.log2repo.AllSlicesLogConverter;
import uk.ac.aston.log2repo.TimeSliceConverter;
import uk.ac.aston.stormlog.Log;

/**
 * Parses RDM log files into EMF resources, using the {@link AllSlicesLogConverter} and
 * {@link TimeSliceConverter} classes. Requires that the RDM configuration file
 * is present in the root folder of the monitored location, with
 * {@link #RDM_JSON} as its filename.
 * 
 * RDM log files should be named following the pattern `log*.json`: they should
 * start with `log` and end with `.json`.
 */
public class RDMLogModelResourceFactory implements IModelResourceFactory {

	private static final String RDM_JSON = "rdm.json";
	@Override
	public String getHumanReadableName() {
		return "RDM Log Parser";
	}

	@Override
	public IHawkModelResource parse(IFileImporter importer, File changedFile) throws Exception {
		File rdmJSON = importer.importFile(RDM_JSON);
		System.out.println();
		if (rdmJSON != null) {
				Log log = new SingleSliceLogConverter(rdmJSON, changedFile).convert();
				ResourceSet rs = new ResourceSetImpl();
				rs.getResourceFactoryRegistry().getExtensionToFactoryMap().put("*", new XMIResourceFactoryImpl());
				Resource r = rs.createResource(URI.createFileURI(changedFile.getAbsolutePath()));
				r.getContents().add(log);
				return new EMFModelResource(r, new EMFWrapperFactory(), this);
		} else {
			return null;
			}
		
	}

	@Override
	public void shutdown() {
		// nothing to do
	}

	@Override
	public boolean canParse(File f) {
		final String fn = f.getName();
		return fn.endsWith(".json") && fn.startsWith("log");
	}

	@Override
	public Collection<String> getModelExtensions() {
		return Collections.singletonList(".json");
	}

}
